package com.mitrais.rms.moduldiscovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class ModulDiscoveryApplication {

	public static void main(String[] args) {
		SpringApplication.run(ModulDiscoveryApplication.class, args);
	}

}
